---
layout: page
title: Presskit
permalink: /presskit/
---

Dayfarers is a turn-based combat strategy game where the player must outmaneuver
brutal opponents by utilizing time travel to turn the tides of battle. When the
battle gets tough, travel through the turns you've taken and use your new
knowledge of the future. With different timelines and unique character
abilities, you can beat any foe that stands in your way!

## Animatic

<figure>
  <iframe
    src="https://www.youtube.com/embed/AGq5vfSGkzk"
    frameborder="0"
    allow="
      accelerometer;
      clipboard-write;
      encrypted-media;
      gyroscope;
      picture-in-picture
    "
    allowfullscreen
    style="width: 100%; height: 480px;"
  ></iframe>
</figure>

## Screenshots

![Promo Image #1](../images/combat1.png)
![Promo Image #2](../images/combat2.png)
![Promo Image #3](../images/combat3.png)

### Logo & Iconography

![Cosmic Engine Logos](../images/CosmicEngineLogoVars.png)
![Dayfarers Logos](../images/DayfarersLogoVariants.png)
