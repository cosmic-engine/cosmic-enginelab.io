---
layout: page
title: About
permalink: /about/
---

![Team](../images/team.jpg)

Cosmic Engine is a Drexel University Senior Project team creating Dayfarers, the
turn-based combat strategy game where YOU control time.

## Social Media

- [Twitter](https://twitter.com/cosmicengineSP)
- [Instagram](https://www.instagram.com/cosmicengineSP)
- [YouTube](https://www.youtube.com/channel/UCO3DDiEFJCOK1rl2jw7lINA)
- [Twitch](https://www.twitch.tv/cosmicenginetv)

## About the Team

Cosmic Engine is a cross-discipline team effort. Six students from the
Antoinette Westphal College of Media Arts & Design, led by Cate Byrd, drive the
artistic vision for the world, characters, and lore of Dayfarers. Likewise, six
students from the College of Computing & Informatics work with development lead
Jacob Ranson to create the systems and mechanics necessary to make Dayfarers
possible. The music and sound design at Cosmic Engine is in the hands of audio
lead Widchard Faustin, and Cosmic Engine's contracted composer and audio
engineer, Julie McLaughlin.

### Design Team

| Member          | Role        | LinkedIn                                                         |
| --------------- | ----------- | ---------------------------------------------------------------- |
| Cate Byrd       | Design Lead | [LinkedIn](https://www.linkedin.com/in/catebyrd/)                |
| Lisa Merola     | Art Lead    | [LinkedIn](https://www.linkedin.com/in/lisa-merola-b40983152/)   |
| Griffin Robbins | Tech Artist | [LinkedIn](https://www.linkedin.com/in/griffin-robbins/)         |
| Ryan Kelley     | 3D Artist   | [LinkedIn](https://www.linkedin.com/in/arkae/)                   |
| Mira Holahan    | 2D Artist   | [LinkedIn](https://www.linkedin.com/in/miraholahan/)             |
| Rex Christian   | Game Artist | [LinkedIn](https://www.linkedin.com/in/rex-christian-a866ba155/) |

### Development Team

| Member            | Role                   | LinkedIn                                                          |
| ----------------- | ---------------------- | ----------------------------------------------------------------- |
| Jacob Ranson      | Development Lead       | [LinkedIn](https://www.linkedin.com/in/jacobrileighranson/)       |
| Conner Pierce     | Tech Lead              | [LinkedIn](https://www.linkedin.com/in/conner-pierce-b12428170/)  |
| Widchard Faustin  | Audio Lead / Developer | [LinkedIn](https://www.linkedin.com/in/w-f-275chs/)               |
| Peter Mangelsdorf | Developer              | [LinkedIn](https://www.linkedin.com/in/peter-mangelsdorf/)        |
| Frank Morrison    | Developer              | [LinkedIn](https://www.linkedin.com/in/frank-morrison-07012a195/) |
| Tom Trimbur       | Developer              | [LinkedIn](https://www.linkedin.com/in/tom-trimbur/)              |

### Contractors

| Member           | Role                      | LinkedIn                                                            |
| ---------------- | ------------------------- | ------------------------------------------------------------------- |
| Julie McLaughlin | Composer / Audio Engineer | [LinkedIn](https://www.linkedin.com/in/julie-mclaughlin-54b818159/) |
