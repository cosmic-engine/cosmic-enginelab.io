---
layout: page
title: Play
permalink: /play/
---

![Promo Image #1](../images/promoposter03.png)

Select your platform:

- [Web Browser](https://gitlab.com/cosmic-engine/dayfarers/-/jobs/artifacts/release/file/build/web/dayfarers.html?job=build_web)
  (unstable)
- [Windows](https://gitlab.com/api/v4/projects/21584998/packages/generic/dayfarers/1.0.0/dayfarers-windows.zip)
- [macOS](https://gitlab.com/api/v4/projects/21584998/packages/generic/dayfarers/1.0.0/dayfarers-mac.zip)
- [Linux](https://gitlab.com/api/v4/projects/21584998/packages/generic/dayfarers/1.0.0/dayfarers-linux.zip)

**Note:** Certain web browsers / hardware setups may not work with the web build
due to bugs in the Godot web exporter. Download the game instead for the best
experience!
